// - Створіть 2 інпути та одну кнопку. Зробіть так, щоб інпути обмінювалися вмістом.

const input1 = document.getElementById("input1");
const input2 = document.getElementById("input2");
const switcher = document.getElementById("switcher");

switcher.onclick = () => {
    let temp1 = input1.value;
    let temp2 = input2.value;
    input2.value = temp1;
    input1.value = temp2;
};

// - Створіть 5 див на сторінці потім використовуючи getElementsByTagName і forEath поміняйте дивам колір фону.

const [...divs] = document.getElementsByTagName("div");

divs.forEach(div => {
    div.style.backgroundColor = randomColor();
});

function randomColor() {
    let color = parseInt(Math.random() * 360);
    return "hsl(" + color + ", 100%, 50%)";
}

// - Створіть багаторядкове поле для введення тексту та кнопки. Після натискання кнопки користувачем програма повинна згенерувати тег div з текстом, який був у багаторядковому полі. багаторядкове поле слід очистити після переміщення інформації

const textarea = document.getElementById("textarea");
const divMaker = document.getElementById("divMaker");
const form = document.getElementById("form");

divMaker.onclick = () => {
    const div = document.createElement("div");
    div.textContent = textarea.value;
    form.append(div);
    textarea.value = "";
};

// - Створіть картинку та кнопку з назвою "Змінити картинку" зробіть так щоб при завантаженні сторінки була картинка https://itproger.com/img/courses/1476977240.jpg
// При натисканні на кнопку вперше картинка замінилася на https://itproger.com/img/courses/1476977488.jpg
// при другому натисканні щоб картинка замінилася на https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png

const image = document.getElementById("image");
const imageChanger = document.getElementById("imageChanger");
const imagesArr = ['https://itproger.com/img/courses/1476977488.jpg','https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png','https://itproger.com/img/courses/1476977240.jpg']

{
    let counter = 0
    imageChanger.onclick = () => {
        i = counter % imagesArr.length;
        image.src = imagesArr[i];
        counter++;
    };
};

// Створіть на сторінці 10 параграфів і зробіть так, щоб при натисканні на параграф він зникав

for (const p of document.getElementsByTagName("p")) {
  p.addEventListener('click', (event) => {
    event.target.remove();
  });
};


// ---Додаткове ДЗ необов'язково---

// Намалювати на сторінці коло за допомогою параметрів, які введе користувач.
// При завантаженні сторінки – показати на ній кнопку з текстом "Намалювати коло". Дана кнопка повинна бути єдиним контентом у тілі HTML документа, решта контенту повинен бути створений і доданий на сторінку за допомогою Javascript
// При натисканні кнопки "Намалювати коло" показувати одне поле введення - діаметр кола. При натисканні на кнопку "Намалювати" створити на сторінці 100 кіл (10*10) випадкового кольору. При натисканні на конкретне коло - це коло повинен зникати, при цьому порожнє місце заповнюватися, тобто всі інші кола зрушуються вліво.

const circleBtn = document.getElementById("circleBtn");
const circles = document.getElementById("circles");

circleBtn.onclick = () => {
    const diametr = prompt("Введіть діаметр", 100) + 'px';

    for (i = 0; i < 100; i++) {
        let circle = document.createElement("div");
        circle.className = "circle"
        circle.style.width = diametr;
        circle.style.height = diametr;
        circle.addEventListener('click', (event) => {
            event.target.remove();
        });
        circle.style.backgroundColor = randomColor();
        circles.appendChild(circle);
    };
};